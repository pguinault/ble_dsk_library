/* =========================================================================
 * This module creates a custom bluetooth service and its attached 
 * characteristics. It can be intentiated multiple times to create multiple 
 * service and services can have misc read/write/notify characteristics of
 * any object size.
 * -------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * You can obtain a non GPL version of this program by contacting me
 * on www.disk91.com
 * --------------------------------------------------------------------------
 * (c) 2015 Paul Pinault / disk91.com
 * --------------------------------------------------------------------------
 * 2015-12-19 - creation
 * ==========================================================================
 * usage :
 * ==========================================================================
 */
 
 #ifndef __BLE_DSK_SERVICE__
 #define __BLE_DSK_SERVICE__
 
#include <stdint.h>
#include <stdbool.h>
#include "ble_dsk_internal.h"
#include "ble.h"
 
 /* ------------------------------------------------------------------------------
  * ble_dsk_characteristic_t - structure that describes a characteristic. readonly
	* ------------------------------------------------------------------------------
	*/
  typedef struct ble_dsk_characteristic_desc_s {
		char												char_description[__BLE_DSK_MAX_STRING_SIZE];		// Text to describe the service
		uint16_t										char_uuid;																			// 16b characteristic UUID 
		uint8_t											char_access_right;															// Access Right bit mask (see BLE_DSK_ACC_XXX)
		uint8_t											char_data_len;																	// Size of the associated data
		uint8_t											char_data_init[__BLE_DSK_MAX_DATA_SIZE];				// table to store the initial value of the data 
		void												(*onWrite)(uint8_t * value);										// callback function on Write Event / NULL if not
		bool												(*onRead)(uint8_t * value);											// callback function on Read Event / NULL if not
																																								// return true if read is authorized , false otherwize ; new value passed on value buffer
		
  } ble_dsk_characteristic_desc_t;
 
 
 /* ------------------------------------------------------------------------------
  * ble_dsk_service_desc_t - structure that defines a service and its associated 
  *                          characteristics. This structure is readonly and can 
  *                          be stored in flash area.
  * ------------------------------------------------------------------------------
  */
  typedef struct ble_dsk_service_desc_s {
	 
	 // Service description Area
	 uint16_t												service_uuid;																		// 16b service UUID  
																																								  //   (what the device show as service number)
	 char														service_description[__BLE_DSK_MAX_STRING_SIZE];	// Text to describe service (max 64 char)
	 
	 // Service / device main callback	
	 void														(*onConnect)();																	// Callback function on Connection
   void														(*onDisconnect)();															// Callback function on Disconnection		
		
	 // Characteristics description Area
	 uint8_t												num_char;																				// Number of characteristics to support in this 
																																									//   service (max __BLE_DSK_MAX_CHARACTERISTICS)
	 ble_dsk_characteristic_desc_t	charact[__BLE_DSK_MAX_CHARACTERISTICS];					// List of characteristics the service will offer
	 
	 	 
	 
  } ble_dsk_service_desc_t;
 
 
 /* ------------------------------------------------------------------------------
	* ble_dsk_acc_xxx - ACCESS TYPE for the charcteristic
	*/
	#define BLE_DSK_ACC_NONE		0																										// Access not defined
	#define BLE_DSK_ACC_READ		1																										// The characteristic can be READ
	#define BLE_DSK_ACC_WRITE		2																										// The characteristic can be WRITE
	#define BLE_DSK_ACC_NOTIFY	4																										// The characteristic can be NOTIFIED
 
	
	/* ------------------------------------------------------------------------------
	 * Structures allou�es dynamiquement
	 * ------------------------------------------------------------------------------
	 */
	typedef struct ble_dsk_dyn_characteristic_s {
		const ble_dsk_characteristic_desc_t 	* char_desc;														// link to the associated characteristic description
		ble_gatts_char_md_t											char_md;															// characteristic MetaData
		ble_gatts_attr_md_t											char_attr;														// characteristic Attribute
		ble_gatts_attr_md_t											char_cccd;														// notify characteristic Attributes
		ble_gatts_attr_t												char_value;														// Attribute of the characteristic value
		ble_uuid_t															char_uuid;														// Charactristic UUID
		uint8_t																  char_data[__BLE_DSK_MAX_DATA_SIZE];		// table to store the value of the data 
		ble_gatts_char_handles_t								char_handler;													// handler for the created characteristic
		
	} ble_dsk_dyn_characteristic_t;
	
	typedef struct ble_dsk_dyn_service_s {
		const ble_dsk_service_desc_t		*		service_desc;															// link to the associated service description
		ble_uuid_t											* 	ble_uuid;																  // service uuid
		
		uint16_t                      			service_handler; 													// gatts given service handler
		uint16_t                      			report_ref_handler;              					// Handler of the Report Reference descriptor.
		uint8_t															chars_handler[__BLE_DSK_MAX_CHARACTERISTICS];	// Index to the characteristic table 
		
	} ble_dsk_dyn_service_t;
	
	
 
 // ----------------------------------------------------------------------------
 // Shared functions & structure
 uint8_t ble_dsk_service_init(void);
 uint8_t ble_dsk_service_allocated(const ble_dsk_service_desc_t * _init, ble_uuid_t * _uuid);
 void ble_dsk_on_event_dispatch(ble_evt_t * p_ble_evt);
 uint32_t ble_dsk_update(const ble_dsk_characteristic_desc_t * _charac, uint8_t * value);
 
 extern ble_dsk_dyn_service_t ble_dsk_dyn_service[__BLE_DSK_MAX_SERVICES];
 extern ble_dsk_dyn_characteristic_t ble_dsk_dyn_characteristic[__BLE_DSK_MAX_TOTAL_CHARACT];

 // ----------------------------------------------------------------------------
 // Internal functions
 static uint8_t ble_dsk_characteristic_allocated(const ble_dsk_characteristic_desc_t * _init);
 static ble_dsk_dyn_characteristic_t * getCharacteristicByValueHandler(uint16_t _handler);
 static ble_dsk_dyn_characteristic_t * getCharacteristicByCharDesc(const ble_dsk_characteristic_desc_t * _caract);
 
 
 #endif // __BLE_DSK_SERVICE__

