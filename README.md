#ble_dsk bletooth library for nrf51 & nrf52

**Author :** Paul Pinault / http://www.disk91.com

**License :** Basically GPL - Non Gpl can be obtained by contacting me.

This library creates custom bluetooth services and its attached
characteristics. It can be intentiated multiple times to create multiple
service and services can have misc read/write/notify characteristics of
any object size.

To use it you just need to create a structure for describing services & characteristics
then call for init. Your interaction will then be made over callback functions. You can
also manually push updates.

You can also add standard ble services like Battery, Device Information, UART and DFU services are not yet implemented
but part of the next evolutions.

more elements and forum : http://www.disk91.com/2015/technology/hardware/nrf-51-ble-library-for-custom-services/

##Requires
Requires Softdevice 110
Memory size : 25.6Kb

##Main files
- **ble_dsk_internal.h** - contains library defines tweek it for save some memory
- **ble_dsk_peripherical.\*** - manage peripherical description and initialization
- **ble_dsk_service.\*** - manage service & characteristics description and initialisation

##Sample code
Here is a description of a ble device with 2 services. The first one have 2 characteristics (one readable
the second one readable and writable). The second srvice have 1 readable characteristic.

The first readable characteristics manage data update on read request when the second manage update on timer basis.

The full source code can be view on main.c file.

```C
const ble_dsk_service_desc_t myServices[2] = {
{
	/* Service UUID */							0x1900,
	/* Service Description */					"testSrv",
	/* onConnect callback */					onConnectTestSrv,
	/* onDisconnect callback */					onDisconnectTestSrv,
	/* Number of characteristics */	2,
	/* Characteristics */					  { 
	{
		 /* Description */								"testChar1",
		 /* Characteristic UUID */						0x2B01,
		 /* Characteristic access right */				(BLE_DSK_ACC_READ | BLE_DSK_ACC_NOTIFY),
		 /* Char Data len	*/							2,
		 /* Char Data initial value */					{0x00,0x01},
		 /* Char onWrite callback   */					NULL,
		 /* Char onRead callback */						onReadTestChar1				// set not NULL will request ReadWrite Auth
	},
	{
		 /* Description */								"testChar2",
		 /* Characteristic UUID */						0x2B02,
		 /* Characteristic access right */				(BLE_DSK_ACC_READ | BLE_DSK_ACC_NOTIFY | BLE_DSK_ACC_WRITE),
		 /* Char Data len	*/							2,
		 /* Char Data initial value */					{0x02,0x03},
		 /* Char onWrite callback   */					onWriteTestChar2,
		 /* Char onRead callback */						NULL,
	}
	}
},
{
	/* Service UUID */									0x1901,
	/* Service Description */							"testSrv2",
	/* onConnect callback */							onConnectTestSrv2,
	/* onDisconnect callback */							onDisconnectTestSrv2,
	/* Number of characteristics */						1,
	/* Characteristics */					  { 
	{
		 /* Description */								"testChar3",
		 /* Characteristic UUID */						0x2B03,
		 /* Characteristic access right */				(BLE_DSK_ACC_READ | BLE_DSK_ACC_NOTIFY),
		 /* Char Data len	*/							4,
		 /* Char Data initial value */					{0x00,0x01,0x00,0x00},
		 /* Char onWrite callback   */					NULL,
		 /* Char onRead callback */						NULL
	}
	}
}
};

const ble_dsk_peripherical_t myPeriph = {
	/* Peripherical name */ 						"disk91Ble",
	/* add_mac_address after name */				false,
	/* Manufacturer name */ 						"disk91.com",
	/* Appearance type */							BLE_APPEARANCE_UNKNOWN,
	/* Basic Service to enable */					BLE_DSK_SERVICE_BAS | BLE_DSK_SERVICE_DIS,
	/* UUID are 128b format */						false,
	/* 128b uuid to be used */						{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	/* Configuration bitfield */			  		BLE_DSK_CONFIG_SERVICE_CHANGE_CHAR,
	
	/* Advertising mode */							(BLE_DSK_ADV_MODE_SLOW | BLE_DSK_ADV_MODE_FAST),
	/* Advertising fast interval ms */				200,
	/* Advertising fast timeout in s */	 			20,		// 20 seconds
	/* Advertising slow interval ms */				600,
	/* Advertising slow timeout in s */				0,		// never ends
	/* Advertizing callback	*/						onAdvertizingChanged,
	/* Advertizing callback stop */					onAdvertizingChanged,

	/* Security options */							BLE_DSK_SEC_DEFAULT_OPTIONS,			// Bonding + MITM
	/* Security io capability */					BLE_DSK_SEC_DEFAULT_IOCAPA,				// None
	/* Security oob data */							BLE_DSK_SEC_DEFAULT_OOBDATA,			// 0
	/* Security min key size */						BLE_DSK_SEC_DEFAULT_MINKEYSZ,			// 7
	/* Security max key size */						BLE_DSK_SEC_DEFAULT_MAXKEYSZ,			// 16
	
	/* GAP min cnx interval */						BLE_DSK_GAP_DEFAULT_MINCNX,				// 100ms
	/* GAP max cnx interval */						BLE_DSK_GAP_DEFAULT_MAXCNX,				// 200ms
	/* GAP slave latency */							BLE_DSK_GAP_DEFAULT_SLAVLAT,			// 0
	/* GAP cnx Supervision timeout */				BLE_DSK_GAP_DEFAULT_CNXSUPTM,			// 4s
	
	/* CNX time for update on connect*/ 			BLE_DSK_CNX_DEFAULT_CNX_TIME,			// 5s
	/* CNX update time */							BLE_DSK_CNX_DEFAULT_CNX_UPDATE, 		// 30s
	/* CNX attempts */								BLE_DSK_CNX_DEFAULT_CNX_TRY,			// 3
	/* CNX disconnect on fail */					BLE_DSK_CNX_DEFAULT_DISC_ON_FAIL, 	// false
	
	/* Battery level function */					getBatteryLevel,
	
	/* Number of custom services */					2,	
	/* Link to custom services desc */				myServices
};
```

Now to use this structure simply add in main :

```C
int main(void) {
	// Initialize timer module.
	APP_TIMER_INIT(APP_TIMER_PRESCALER, 4, false);

	// Init the ble structure
	ble_dsk_peripherical_init(&myPeriph);
	
	for (;;) {
    sd_app_evt_wait();
  }
}
```

You will later have to create the callback functions:

```C
void onAdvertizingChanged(uint8_t event) {
		if ( event == BLE_ADV_EVT_IDLE ) {
			sd_power_system_off();
		}
}
	
uint8_t getBatteryLevel() { return 0x10; }
void onConnectTestSrv(void) {}
void onDisconnectTestSrv(void) {}
void onConnectTestSrv2(void) {}
void onDisconnectTestSrv2(void) {}
bool onReadTestChar1(uint8_t * data_buf) {
	uint16_t * _v = (uint16_t *) data_buf;
	*_v = (*_v+1);
	return true;
}
void onWriteTestChar2(uint8_t * v) {}
```