/* =========================================================================
 * This module creates a custom bluetooth service and its attached 
 * characteristics. It can be intentiated multiple times to create multiple 
 * service and services can have misc read/write/notify characteristics of
 * any object size.
 * -------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * You can obtain a non GPL version of this program by contacting me
 * on www.disk91.com
 * --------------------------------------------------------------------------
 * (c) 2015 Paul Pinault / disk91.com
 * --------------------------------------------------------------------------
 * 2015-12-19 - creation
 * ==========================================================================
 * usage :
 * ==========================================================================
 */
 
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "ble_dsk_peripherical.h"
#include "ble_dsk_service.h"
#include "ble_srv_common.h"
#include "ble_gatts.h"
#include "app_error.h" 

/* -----------------------------------------------------------------------
 * global structure to store service & characteristics informations
 * -----------------------------------------------------------------------
 */
ble_dsk_dyn_service_t ble_dsk_dyn_service[__BLE_DSK_MAX_SERVICES];
ble_dsk_dyn_characteristic_t ble_dsk_dyn_characteristic[__BLE_DSK_MAX_TOTAL_CHARACT]; 
static int last_service = 0;						// index to the last service structure used
static int last_characteristic = 0;			// index to the last characteristic used

/* ---------------------------------------------------------------------
 * Allocate BLE service structure and associate uuid
 * ---------------------------------------------------------------------
 */
uint8_t ble_dsk_service_allocated(const ble_dsk_service_desc_t * _init, ble_uuid_t * _uuid) {
	  uint8_t	ret;
		// ---------------------------------------
	  // Init for service structure
  	if ( last_service < __BLE_DSK_MAX_SERVICES ) {
			ble_dsk_dyn_service_t * _s = &ble_dsk_dyn_service[last_service];
			memset(_s, 0, sizeof(ble_dsk_dyn_service_t));
			
			_s->service_desc = _init;
			_s->ble_uuid = _uuid;
			ret = last_service;
			last_service++;
		} else ret = __BLE_DSK_ERROR_ERROR;
		return ret;
}

/* ---------------------------------------------------------------------
 * Allocate BLE characteristic structure 
 * ---------------------------------------------------------------------
 */
static uint8_t ble_dsk_characteristic_allocated(const ble_dsk_characteristic_desc_t * _init) {
	  uint8_t	ret;
		// ---------------------------------------
	  // Init for characteristic structure
  	if ( last_characteristic < __BLE_DSK_MAX_TOTAL_CHARACT ) {
			ble_dsk_dyn_characteristic_t * _c = &ble_dsk_dyn_characteristic[last_characteristic];
			memset(_c, 0, sizeof(ble_dsk_dyn_characteristic_t));
			
			_c->char_desc = _init;
			ret = last_characteristic;
			last_characteristic++;
		} else ret = __BLE_DSK_ERROR_ERROR;
		return ret;	
}


/* ---------------------------------------------------------------------
 * Init all Ble Services
 * ---------------------------------------------------------------------
 */
uint8_t ble_dsk_service_init() {
	int i,j;
  uint32_t   err_code;
	for ( i = 0 ; i < last_service ; i++ ) {
		ble_dsk_dyn_service_t * _s = &ble_dsk_dyn_service[i];
		
		// -----------------------------------------------------------------
		// Register service
		_s->report_ref_handler = BLE_CONN_HANDLE_INVALID;
		err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, _s->ble_uuid ,&_s->service_handler );
    APP_ERROR_CHECK(err_code);
		
		// -----------------------------------------------------------------
		// Register characteristics
		for ( j = 0 ; j < _s->service_desc->num_char ; j++ ) {
			 int idx_char = ble_dsk_characteristic_allocated(&_s->service_desc->charact[j]);
			 if ( idx_char != __BLE_DSK_ERROR_ERROR ) {
				 _s->chars_handler[j] = idx_char;
				 ble_dsk_dyn_characteristic_t * _c = &ble_dsk_dyn_characteristic[idx_char];
				 
				 
				 // check if notify is configured
				 // @ TODO - revoir avec les notions de securit� ...
				 //BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&cccd_md.read_perm); <- a priori ne peut pas etre securis�
         //BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&cccd_md.write_perm);
				 if ( (_c->char_desc->char_access_right & BLE_DSK_ACC_NOTIFY) != 0 ) {
 					 BLE_GAP_CONN_SEC_MODE_SET_OPEN(&_c->char_cccd.read_perm);
					 BLE_GAP_CONN_SEC_MODE_SET_OPEN(&_c->char_cccd.write_perm);
					 _c->char_cccd.vloc = BLE_GATTS_VLOC_STACK;
					 _c->char_md.char_props.notify = 1;
					 _c->char_md.p_cccd_md = &_c->char_cccd;
				 } else {
					 _c->char_md.char_props.notify = 0;
					 _c->char_md.p_cccd_md = NULL;
				 }
				 
				 // Create access metadata
				 if ( (_c->char_desc->char_access_right & BLE_DSK_ACC_READ) != 0 ) {
						_c->char_md.char_props.read = 1;
					  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&_c->char_attr.read_perm);
				 } else {
					  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&_c->char_attr.read_perm);
				 }
				 if ( (_c->char_desc->char_access_right & BLE_DSK_ACC_WRITE) != 0 ) {
						_c->char_md.char_props.write = 1;
					  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&_c->char_attr.write_perm);
				 } else {
					  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&_c->char_attr.write_perm);
				 }
				 
				 // Other Misc metdata
				 _c->char_md.p_char_user_desc = NULL;
				 _c->char_md.p_char_pf 			  = NULL;
				 _c->char_md.p_user_desc_md   = NULL;
				 _c->char_md.p_sccd_md				= NULL;

				 // Create attributes
				 _c->char_attr.vloc    = BLE_GATTS_VLOC_STACK;
				 _c->char_attr.rd_auth = 0;
				 _c->char_attr.wr_auth = 0;
				 _c->char_attr.vlen    = 0;
				 _c->char_attr.rd_auth = (_c->char_desc->onRead != NULL)?1:0;
				 _c->char_attr.wr_auth = 0;	// actually not managed
			
				 // Create value attribute
				 BLE_UUID_BLE_ASSIGN(_c->char_uuid, _c->char_desc->char_uuid);
				 _c->char_value.p_uuid    = &_c->char_uuid;
				 _c->char_value.p_attr_md = &_c->char_attr;
				 _c->char_value.init_len  = _c->char_desc->char_data_len;
				 _c->char_value.init_offs = 0;
				 _c->char_value.max_len   = _c->char_desc->char_data_len;
				 memcpy(_c->char_data,_c->char_desc->char_data_init,_c->char_desc->char_data_len);
				 _c->char_value.p_value   = _c->char_data;

				 // Create characteristic
			   err_code = sd_ble_gatts_characteristic_add( _s->service_handler, &_c->char_md, &_c->char_value, &_c->char_handler); 
				 APP_ERROR_CHECK(err_code);
				 
			 } else {
				 APP_ERROR_CHECK(NRF_ERROR_FORBIDDEN);
			 }
		}
	}	
	return NRF_SUCCESS;
}

/* -----------------------------------------------------------------------------------
 * External update request
 * -----------------------------------------------------------------------------------
 */
 uint32_t ble_dsk_update(const ble_dsk_characteristic_desc_t * _charac, uint8_t * value) {
	 
	  uint32_t err_code = NRF_SUCCESS;
		ble_dsk_dyn_characteristic_t * _c = getCharacteristicByCharDesc(_charac);
	  
	  if ( _c != NULL ) {
			// copy new value
			memcpy(_c->char_data,value,_c->char_desc->char_data_len);
			// send new value
			ble_gatts_value_t _value;
      memset(&_value, 0, sizeof(_value));
			_value.len 	   = _c->char_desc->char_data_len;
			_value.offset  = 0;
			_value.p_value = _c->char_data;
			err_code = sd_ble_gatts_value_set(ble_dsk_dyn_peripherical.m_conn_handle,
																				_c->char_handler.value_handle,
																				&_value);
			// Notify
			if (    ble_dsk_dyn_peripherical.m_conn_handle != BLE_CONN_HANDLE_INVALID
				   && ( _c->char_desc->char_access_right & BLE_DSK_ACC_NOTIFY ) != 0 ) {
						 
						ble_gatts_hvx_params_t hvx_params;
            memset(&hvx_params, 0, sizeof(hvx_params));

            hvx_params.handle =  _c->char_handler.value_handle;
            hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
            hvx_params.offset = _value.offset;
            hvx_params.p_len  = &_value.len;
            hvx_params.p_data = _value.p_value;
            err_code = sd_ble_gatts_hvx(ble_dsk_dyn_peripherical.m_conn_handle, &hvx_params);
			} else {
				return NRF_ERROR_INVALID_STATE;
			}
			
		} else {
        return NRF_ERROR_NULL;
		}			
	  return err_code;
}
	       

/* -----------------------------------------------------------------------------------
 * Event Management
 * -----------------------------------------------------------------------------------
 */

/* ble_dsk_on_event_dispatch
 * dispatch the event to the associated callback
 */
void ble_dsk_on_event_dispatch(ble_evt_t * p_ble_evt) 
{

		switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTS_EVT_WRITE:
					   {
								ble_gatts_evt_write_t * p_evt_write = &(p_ble_evt->evt.gatts_evt.params.write);
							  ble_dsk_dyn_characteristic_t * _c = getCharacteristicByValueHandler(p_evt_write->handle);
								if ( _c != NULL ) {
										if ( p_evt_write->len == _c->char_desc->char_data_len ) {
												memcpy(_c->char_data,p_evt_write->data,_c->char_desc->char_data_len);
											  _c->char_desc->onWrite(_c->char_data);
										}
								}
						 }
             break;

				// --------------------------------------------------------------------------
				// This is a way to manage onRead event firing - request for an authorize
				// before any read when the callback is defined. The function will return a
				// new value and an authorzation (or not)
				case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
					   {
							  if ( p_ble_evt->evt.gatts_evt.params.authorize_request.type == BLE_GATTS_AUTHORIZE_TYPE_READ ) {
							    uint16_t _handler = p_ble_evt->evt.gatts_evt.params.authorize_request.request.read.context.value_handle;
							    ble_dsk_dyn_characteristic_t * _c = getCharacteristicByValueHandler(_handler);
									if ( _c != NULL ) {
										ble_gatts_rw_authorize_reply_params_t reply;
										memset(&reply,0,sizeof(reply));
										reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
										if (_c->char_desc->onRead(_c->char_data) ) {
											reply.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
											reply.params.read.update = 1;
											reply.params.read.offset = 0;
											reply.params.read.len    = _c->char_desc->char_data_len;
											reply.params.read.p_data = _c->char_data;
										} else {
											reply.params.read.gatt_status = BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED;
										}
										sd_ble_gatts_rw_authorize_reply(ble_dsk_dyn_peripherical.m_conn_handle ,&reply);
									}
								}
						 }
						 break;
				
        default:
            // No implementation needed.
            break;
    }
}
	
/* -----------------------------------------------------------------------------------
 * misc internal function
 * -----------------------------------------------------------------------------------
 */
					 
static ble_dsk_dyn_characteristic_t * getCharacteristicByValueHandler(uint16_t _handler) {
		int i;
		for ( i = 0 ; i < last_characteristic ; i++ ) {
				if ( ble_dsk_dyn_characteristic[i].char_handler.value_handle == _handler )
					return &ble_dsk_dyn_characteristic[i];
		}
		return NULL;
}

static ble_dsk_dyn_characteristic_t * getCharacteristicByCharDesc(const ble_dsk_characteristic_desc_t * _caract) {
		int i;
		for ( i = 0 ; i < last_characteristic ; i++ ) {
				if ( ble_dsk_dyn_characteristic[i].char_desc == _caract )
					return &ble_dsk_dyn_characteristic[i];
		}
		return NULL;
}

