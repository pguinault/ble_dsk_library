/* =========================================================================
 * This module is initializing a ble_peripherical device from a simple
 * given structure and create all the needed services & charcarteristics
 * -------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * You can obtain a non GPL version of this program by contacting me
 * on www.disk91.com
 * --------------------------------------------------------------------------
 * (c) 2015 Paul Pinault / disk91.com
 * --------------------------------------------------------------------------
 * 2015-12-19 - creation
 * ==========================================================================
 * usage :
 * ==========================================================================
 */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "app_timer.h"
#include "ble_hci.h"
#include "ble_dsk_beacon.h"
#include "softdevice_handler.h"
#include "nrf_delay.h"

#define CENTRAL_LINK_COUNT               0                                          /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT            1  


/* ---------------------------------------------------------------------
 * Global variable with all elements on peripherical
 * ---------------------------------------------------------------------
 */
ble_dsk_dyn_beacon_t	ble_dsk_dyn_beacon;
static ble_dsk_dyn_beacon_t * _b = &ble_dsk_dyn_beacon;

/* ---------------------------------------------------------------------
 * Init Ble Peripherical
 * ---------------------------------------------------------------------
 */
void ble_dsk_beacon_init(const ble_dsk_beacon_t * _init)
{
    uint32_t err_code;

	// --------------------------------------------
	// create and initialize the ble_dsk_dyn_periph structure
	_b->beacon = _init;
		
	// --------------------------------------------
	// init softdevice (req - softdevice_handler.h) 
    uint8_t enabled;
	sd_softdevice_is_enabled(&enabled);
	if(!enabled)SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_RC_250_PPM_8000MS_CALIBRATION, NULL);

#if defined(S110) || defined(S130) || defined(S132)
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
#if (defined(S130) || defined(S132))
    ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
#endif

	err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
													PERIPHERAL_LINK_COUNT,
													&ble_enable_params);
	APP_ERROR_CHECK(err_code);

	//Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
		
	// Enable BLE stack.	
	if(!enabled) {	
		err_code = softdevice_enable(&ble_enable_params);
		APP_ERROR_CHECK(err_code);
	}
#endif

	// Manuf data init
	{
		_b->manuf_specific_data.company_identifier = _b->beacon->company_identifier;

		_b->manuf_specific_data.data.p_data = (uint8_t *)_b->beacon->beacon_info;
		_b->manuf_specific_data.data.size   = _b->beacon->info_length;
	}
	
	// --------------------------------------------------
	// Advertizing init
	{
		memset(&_b->adv_data, 0, sizeof(_b->adv_data));
		
		_b->adv_data.name_type             = BLE_ADVDATA_NO_NAME;
		_b->adv_data.flags                 = BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED;
		_b->adv_data.p_manuf_specific_data = &_b->manuf_specific_data;
	}
		
	err_code = ble_advdata_set(&_b->adv_data, NULL);
    APP_ERROR_CHECK(err_code);
	
	memset(&_b->adv_params, 0, sizeof(_b->adv_params));

    _b->adv_params.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
    _b->adv_params.p_peer_addr = NULL;                             // Undirected advertisement.
    _b->adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    _b->adv_params.interval    = NON_CONNECTABLE_ADV_INTERVAL;
    _b->adv_params.timeout     = APP_CFG_NON_CONN_ADV_TIMEOUT;
			
	err_code = sd_ble_gap_adv_start(&_b->adv_params);
    APP_ERROR_CHECK(err_code);
}

void advertising_start() {
	uint8_t err_code = sd_ble_gap_adv_start(&_b->adv_params);
    APP_ERROR_CHECK(err_code);
}

void adv_stop() {
	sd_ble_gap_adv_stop();
}

void set_minor(uint16_t minor)
{
	_b->beacon->beacon_info[20] = MSB_16(minor);
	_b->beacon->beacon_info[21] = LSB_16(minor);
	
	uint8_t err_code = ble_advdata_set(&_b->adv_data, NULL);
    APP_ERROR_CHECK(err_code);
}

void set_major(uint16_t major)
{
	_b->beacon->beacon_info[18] = MSB_16(major);
	_b->beacon->beacon_info[19] = LSB_16(major);
	
	uint8_t err_code = ble_advdata_set(&_b->adv_data, NULL);
    APP_ERROR_CHECK(err_code);
}

void set_code(uint16_t major, uint16_t minor)
{
	_b->beacon->beacon_info[18] = MSB_16(major);
	_b->beacon->beacon_info[19] = LSB_16(major);
	_b->beacon->beacon_info[20] = MSB_16(minor);
	_b->beacon->beacon_info[21] = LSB_16(minor);
	
	uint8_t err_code = ble_advdata_set(&_b->adv_data, NULL);
    APP_ERROR_CHECK(err_code);
}

void set_uuid(const uint8_t * uuid) {
	memcpy(_b->beacon->beacon_info+2, uuid, 16);
	uint8_t err_code = ble_advdata_set(&_b->adv_data, NULL);
    APP_ERROR_CHECK(err_code);
}

void set_last_uuid(const uint8_t val) {
	_b->beacon->beacon_info[17] = val | 0xF0;
	uint8_t err_code = ble_advdata_set(&_b->adv_data, NULL);
    APP_ERROR_CHECK(err_code);
}
