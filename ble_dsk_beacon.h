/* =========================================================================
 * This module is initializing a ble_peripherical device from a simple
 * given structure and create all the needed services & charcarteristics
 * -------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * You can obtain a non GPL version of this program by contacting me
 * on www.disk91.com
 * --------------------------------------------------------------------------
 * (c) 2016 Pierre Guinault / guinault.info
 * --------------------------------------------------------------------------
 * 2016-10-27 - creation
 * ==========================================================================
 * usage :
 * ==========================================================================
 */
 #ifndef __BLE_DSK_BEACON__
 #define __BLE_DSK_BEACON__

#include "ble_gattc.h"

 #include <stdint.h>
 #include <stdbool.h>
 #include "ble_dsk_internal.h"
 #include "ble_dsk_service.h"
 #include "ble.h"
 #include "ble_advdata.h"
 #include "ble_advertising.h"
 #include "ble_conn_params.h"
 #include "device_manager.h"
 #include "pstorage.h"



 /* ----------------------------------------------------------------------
  * Initialization structure - this structure defines how the beacon
	*                            will be created / initialized ; this structure
	*                            is readonly to be stored in flash
	* ----------------------------------------------------------------------
	*/
 typedef struct ble_dsk_beacon_s {
	 
	ble_uuid128_t	nus_base_uuid;																// 128b UUID base (only needed if uuid_128 = true) Format = { xx,xx,xx ... x16}
																																				// See - http://www.itu.int/en/ITU-T/asn1/Pages/UUID/generate_uuid.aspx 
																											// BitField for different configuration see (BLE_DSK_CONFIG_XXX)	
	// advertising parameters					// type of advertising (direct (automatically reconnect) / slow (preserve nrj / fast (default))
	uint8_t		advertising_interval;		// Time in ms between two ble advertisement (direct&fast mode)
	uint8_t		advertising_timeout;		// advertisement duration (then stop) in second - 0 for unlimited // # of time if direct mode
	uint16_t	company_identifier;
	// Beacon info
	uint8_t				info_length; 		// Info length
	uint8_t		*	beacon_info; 		// Storage for beacon info
	 
 } ble_dsk_beacon_t;
 
 /* ----------------------------------------------------------------------
  * some default values
  * ----------------------------------------------------------------------
  */
#define NON_CONNECTABLE_ADV_INTERVAL	MSEC_TO_UNITS(100, UNIT_0_625_MS)																			// 50ms advertising 
#define APP_CFG_NON_CONN_ADV_TIMEOUT	0								
#define APP_BEACON_INFO_LENGTH          0x17  
#define APP_ADV_DATA_LENGTH             0x15                              /**< Length of manufacturer specific data in the advertisement. */
 /* ----------------------------------------------------------------------
  * Internal peipherical structure
	* ----------------------------------------------------------------------
	*/
typedef struct ble_dsk_dyn_beacon_s {
	
	// configuration
 	const ble_dsk_beacon_t *		beacon;												// const initialization structure
	 																														// bonding (encryption) information from pstorage
	// advertising data
	ble_advdata_t 					adv_data;														// advertising configuration
	ble_gap_adv_params_t 			adv_params;

	ble_advdata_manuf_data_t 		manuf_specific_data;																																						// limited to peripherical->service_count

	
} ble_dsk_dyn_beacon_t;
 
extern ble_dsk_dyn_beacon_t	ble_dsk_dyn_beacon;
 

/* -----------------------------------------------------------------------
 * External sub functions
 * -----------------------------------------------------------------------
 */
void ble_dsk_beacon_init(const ble_dsk_beacon_t * _init);
void advertising_start(void);
void adv_stop(void);
void set_minor(uint16_t minor);
void set_major(uint16_t major);
void set_code(uint16_t major, uint16_t minor);
void set_uuid(const uint8_t * uuid);
void set_last_uuid(const uint8_t val);
/* -----------------------------------------------------------------------
 * internal sub functions
 * -----------------------------------------------------------------------
 */

#endif 	// __BLE_DSK_BEACON__
