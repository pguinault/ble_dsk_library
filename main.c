/* =========================================================================
 * This file is a sample exemple of use for ble_dsk library
 * -------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * You can obtain a non GPL version of this program by contacting me
 * on www.disk91.com
 * --------------------------------------------------------------------------
 * (c) 2015 Paul Pinault / disk91.com
 * --------------------------------------------------------------------------
 * 2015-12-19 - creation
 * ==========================================================================
 * usage :
 * ==========================================================================
 */
#include <stdint.h>
#include <string.h>
#include "nrf_soc.h"
#include "app_timer.h"
#include "ble_dsk_peripherical.h"
#include "ble_dsk_beacon.h"

#ifndef MODE
#define MODE CONN	/** CONN : BLE connection with services and characteristics, BEACON : no connection */
#endif


#if MODE == BEACON
#define APP_DEVICE_TYPE                 0x02                              /**< 0x02 refers to Beacon. */
#define APP_MEASURED_RSSI               0xC3                              /**< The Beacon's measured RSSI at 1 meter distance in dBm. */
#define APP_COMPANY_IDENTIFIER          0x0059                            /**< Company identifier for Nordic Semiconductor ASA. as per www.bluetooth.org. */
#define APP_MAJOR_VALUE                 0x00, 0x00                       /**< Major value used to identify Beacons. */ 
#define APP_MINOR_VALUE                 0x00, 0x00
#define APP_BEACON_UUID0                 0x01, 0x12, 0x23, 0x34, \
                                        0x45, 0x56, 0x67, 0x78, \
                                        0x89, 0x9a, 0xab, 0xbc, \
                                        0xcd, 0xde, 0xef, 0xf0
#endif

#if MODE == CONN
APP_TIMER_DEF(timer_1);

void onAdvertizingChanged(uint8_t event);
uint8_t getBatteryLevel(void);
void onConnectTestSrv(void);
void onDisconnectTestSrv(void);
void onConnectTestSrv2(void);
void onDisconnectTestSrv2(void);
bool onReadTestChar1(uint8_t * data_buf);
void onWriteTestChar2(uint8_t * v);

const ble_dsk_service_desc_t myServices[2] = {
{
	/* Service UUID */							0x1900,
	/* Service Description */				"testSrv",
	/* onConnect callback */				onConnectTestSrv,
	/* onDisconnect callback */				onDisconnectTestSrv,
	/* Number of characteristics */	2,
	/* Characteristics */					  { 
	{
		 /* Description */									"testChar1",
		 /* Characteristic UUID */					0x2B01,
		 /* Characteristic access right */	(BLE_DSK_ACC_READ | BLE_DSK_ACC_NOTIFY),
		 /* Char Data len	*/								2,
		 /* Char Data initial value */			{0x00,0x01},
		 /* Char onWrite callback   */			NULL,
		 /* Char onRead callback */					onReadTestChar1				// set not NULL will request ReadWrite Auth
	},
	{
		 /* Description */									"testChar2",
		 /* Characteristic UUID */					0x2B02,
		 /* Characteristic access right */	(BLE_DSK_ACC_READ | BLE_DSK_ACC_NOTIFY | BLE_DSK_ACC_WRITE),
		 /* Char Data len	*/								2,
		 /* Char Data initial value */			{0x02,0x03},
		 /* Char onWrite callback   */			onWriteTestChar2,
		 /* Char onRead callback */					NULL,
	}
	}
},
{
	/* Service UUID */							0x1901,
	/* Service Description */				"testSrv2",
	/* onConnect callback */				onConnectTestSrv2,
	/* onDisconnect callback */			onDisconnectTestSrv2,
	/* Number of characteristics */	1,
	/* Characteristics */					  { 
	{
		 /* Description */									"testChar3",
		 /* Characteristic UUID */					0x2B03,
		 /* Characteristic access right */	(BLE_DSK_ACC_READ | BLE_DSK_ACC_NOTIFY),
		 /* Char Data len	*/								4,
		 /* Char Data initial value */			{0x00,0x01,0x00,0x00},
		 /* Char onWrite callback   */			NULL,
		 /* Char onRead callback */					NULL
	}
	}
}
};

const ble_dsk_peripherical_t myPeriph = {
	/* Peripherical name */ 						"disk91Ble",
	/* add_mac_address after name */		false,
	/* Manufacturer name */ 						"disk91.com",
	/* Appearance type */								BLE_APPEARANCE_UNKNOWN,	
	/* TX power level */								0,   // Value expected : 4,0,-4,-8,-16,-20,-30(NRF51),-40(NRF52)
	/* Basic Service to enable */				BLE_DSK_SERVICE_BAS | BLE_DSK_SERVICE_DIS | BLE_DSK_SERVICE_DFU,
	/* UUID are 128b format */					false,
	/* 128b uuid to be used */					{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	/* Configuration bitfield */			  BLE_DSK_CONFIG_SERVICE_CHANGE_CHAR,
	
	/* Advertising mode */							(BLE_DSK_ADV_MODE_SLOW | BLE_DSK_ADV_MODE_FAST),
	/* Advertising fast interval ms */	200,
	/* Advertising fast timeout in s */	 20,		// 20 seconds
	/* Advertising slow interval ms */	600,
	/* Advertising slow timeout in s */		0,		// never ends
	/* Advertizing callback	*/					onAdvertizingChanged,
	/* Advertizing callback stop */			onAdvertizingChanged,

	/* Security options */							BLE_DSK_SEC_DEFAULT_OPTIONS,			// Bonding + MITM
	/* Security io capability */				BLE_DSK_SEC_DEFAULT_IOCAPA,				// None
	/* Security oob data */							BLE_DSK_SEC_DEFAULT_OOBDATA,			// 0
	/* Security min key size */					BLE_DSK_SEC_DEFAULT_MINKEYSZ,			// 7
	/* Security max key size */					BLE_DSK_SEC_DEFAULT_MAXKEYSZ,			// 16
	
	/* GAP min cnx interval */					BLE_DSK_GAP_DEFAULT_MINCNX,				// 100ms
	/* GAP max cnx interval */					BLE_DSK_GAP_DEFAULT_MAXCNX,				// 200ms
	/* GAP slave latency */							BLE_DSK_GAP_DEFAULT_SLAVLAT,			// 0
	/* GAP cnx Supervision timeout */		BLE_DSK_GAP_DEFAULT_CNXSUPTM,			// 4s
	
	/* CNX time for update on connect*/ BLE_DSK_CNX_DEFAULT_CNX_TIME,			// 5s
	/* CNX update time */								BLE_DSK_CNX_DEFAULT_CNX_UPDATE, 	// 30s
	/* CNX attempts */									BLE_DSK_CNX_DEFAULT_CNX_TRY,			// 3
	/* CNX disconnect on fail */				BLE_DSK_CNX_DEFAULT_DISC_ON_FAIL, // false
	
	/* Battery level function */				getBatteryLevel,
	
	/* Number of custom services */			2,	
	/* Link to custom services desc */	myServices
};
#endif

#if MODE == BEACON
uint8_t beacon_info[APP_BEACON_INFO_LENGTH] = {
	APP_DEVICE_TYPE,     // Manufacturer specific information. Specifies the device type in this 
                         // implementation. 
    APP_ADV_DATA_LENGTH, // Manufacturer specific information. Specifies the length of the 
                         // manufacturer specific data in this implementation.
    APP_BEACON_UUID0,     // 128 bit UUID value. 
    APP_MAJOR_VALUE,     // Major arbitrary value that can be used to distinguish between Beacons. 
    APP_MINOR_VALUE,     // Minor arbitrary value that can be used to distinguish between Beacons. 
    APP_MEASURED_RSSI    // Manufacturer specific information. The Beacon's measured TX power in 
                         // this implementation. 
};

const ble_dsk_beacon_t beacon = {
	APP_BEACON_UUID0,
	NON_CONNECTABLE_ADV_INTERVAL,
	APP_CFG_NON_CONN_ADV_TIMEOUT,
	APP_COMPANY_IDENTIFIER,
	APP_BEACON_INFO_LENGTH,
	beacon_info
};
#endif

#if MODE == CONN
void onAdvertizingChanged(uint8_t event) {
		if ( event == BLE_ADV_EVT_IDLE ) {
			sd_power_system_off();
		}
}
	
uint8_t getBatteryLevel() {
	return 0x10;
}

void onConnectTestSrv(void) {

}

void onDisconnectTestSrv(void) {

}
void onConnectTestSrv2(void) {

}

void onDisconnectTestSrv2(void) {

}
bool onReadTestChar1(uint8_t * data_buf) {
	uint16_t * _v = (uint16_t *) data_buf;
	*_v = (*_v+1);
	return true;
}


void onWriteTestChar2(uint8_t * v) {
	
}

uint32_t	val32 = 0;  
static void timer1_timeout_handler(void * p_context) {
  val32++;	
	if ( ble_dsk_update(&myPeriph.services[1].charact[0], (uint8_t *)&val32 ) == NRF_SUCCESS ) {
		// well done
	}
}

#endif


int main(void) {
	// Initialize timer module.
	APP_TIMER_INIT(APP_TIMER_PRESCALER, 4, false);
#if MODE == CONN
	app_timer_create(&timer_1,APP_TIMER_MODE_REPEATED, timer1_timeout_handler);

	// Init the ble structure
	ble_dsk_peripherical_init(&myPeriph);
	
	// run timer
	app_timer_start(timer_1, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
#endif 
#if MODE == BEACON
	ble_dsk_beacon_init(&beacon);
#endif
	
	for (;;) {
		sd_app_evt_wait();
	}
}


