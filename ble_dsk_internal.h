/* =========================================================================
 * This module creates a custom bluetooth service and its attached 
 * characteristics. It can be intentiated multiple times to create multiple 
 * service and services can have misc read/write/notify characteristics of
 * any object size.
 * -------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * You can obtain a non GPL version of this program by contacting me
 * on www.disk91.com
 * --------------------------------------------------------------------------
 * (c) 2015 Paul Pinault / disk91.com
 * --------------------------------------------------------------------------
 * 2015-12-19 - creation
 * ==========================================================================
 * usage : ble_dsk_internal contains internal structure and constants
 * ==========================================================================
 */
 #ifndef __BLE_DSK_INTERNAL__
 #define __BLE_DSK_INTERNAL__
 
 #define __BLE_DSK_MAX_SERVICES						10						// maximum number of services the structure can manage	
 #define __BLE_DSK_MAX_CHARACTERISTICS		 5						// maximum characteristics per services the structures can manage
 #define __BLE_DSK_MAX_TOTAL_CHARACT		 	20						// maximum characteristics the library will manage ... overall
 #define __BLE_DSK_MAX_STRING_SIZE			  64						// maximum size of the String type structures
 #define __BLE_DSK_MAX_DATA_SIZE					16						// maximum size of any of the data exchanged over the characteristics
 
#if WITH_DFU_SERVICE == 1
	#define IS_SRVC_CHANGED_CHARACT_PRESENT		1

	#define DFU_REV_MAJOR                    	0x00                                       /** DFU Major revision number to be exposed. */
	#define DFU_REV_MINOR                    	0x01                                       /** DFU Minor revision number to be exposed. */
	#define DFU_REVISION                     	((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
	#define APP_SERVICE_HANDLE_START         	0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
	#define BLE_HANDLE_MAX                   	0xFFFF                                     /**< Max handle value in BLE. */

#endif // BLE_DFU_APP_SUPPORT
 
 #ifndef APP_TIMER_PRESCALER
 #define APP_TIMER_PRESCALER              0             // Value of the RTC1 PRESCALER register.
 #endif
 
 #define __BLE_DSK_ERROR_ERROR						0xFF					// return value indicating an error
 
// Modes 
#define CONN 0
#define BEACON 1
 
 #endif // __BLE_DSK_INTERNAL__
 
